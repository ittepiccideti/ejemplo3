package com.androidbeginners.israelarjonavizcaino.ejemplo3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnAritmetica, btnConversion, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void irVentanaAritmetica(View v) {
        Intent otraVentana = new Intent(this, AritmeticActivity.class);
        startActivity(otraVentana);

    }

    public void irVentanaConversion(View v) {
        Intent otraVentana = new Intent(this, ConversionActivity.class);
        startActivity(otraVentana);
    }

    public void salir(View v){
        finish();
    }
}

package com.androidbeginners.israelarjonavizcaino.ejemplo3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AritmeticActivity extends AppCompatActivity {

    EditText num1, num2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aritmetic);

        num1 = (EditText) findViewById(R.id.etValor1);
        num2 = (EditText) findViewById(R.id.etValor2);
    }

    public void sumar(View v) {
        try {
            int n1 = Integer.parseInt(num1.getText().toString());
            int n2= Integer.parseInt(num2.getText().toString());
            int suma = n2 + n1;

            Toast.makeText(this, "La suma es "+suma, Toast.LENGTH_LONG).show();

            num1.setText("");
            num2.setText("");

        } catch (Exception e) {
            Toast.makeText(this, "Debes capturar ambos números", Toast.LENGTH_LONG).show();
        }
    }

    public void restar(View v) {
        try {
            int n1 = Integer.parseInt(num1.getText().toString());
            int n2= Integer.parseInt(num2.getText().toString());
            int resta = n2 - n1;

            Toast.makeText(this, "La resta es "+resta, Toast.LENGTH_LONG).show();

            num1.setText("");
            num2.setText("");

        } catch (Exception e) {
            Toast.makeText(this, "Debes capturar ambos números", Toast.LENGTH_LONG).show();
        }
    }

    public void multiplicar(View v) {
        try {
            int n1 = Integer.parseInt(num1.getText().toString());
            int n2= Integer.parseInt(num2.getText().toString());
            int res = n2 * n1;

            Toast.makeText(this, "La multiplicación es "+res, Toast.LENGTH_LONG).show();

            num1.setText("");
            num2.setText("");

        } catch (Exception e) {
            Toast.makeText(this, "Debes capturar ambos números", Toast.LENGTH_LONG).show();
        }
    }

    public void dividir(View v) {
        try {
            int n1 = Integer.parseInt(num1.getText().toString());
            int n2= Integer.parseInt(num2.getText().toString());
            int res = n2 / n1;

            Toast.makeText(this, "La división es "+res, Toast.LENGTH_LONG).show();

            num1.setText("");
            num2.setText("");

        } catch (Exception e) {
            Toast.makeText(this, "Ocurrió un error", Toast.LENGTH_LONG).show();
        }
    }
}
